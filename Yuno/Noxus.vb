﻿Module Noxus
    Public Structure Member

        Private vName As String
        Private vLastName As String

        Public Property Name() As String
            Get
                Return vName
            End Get
            Set(value As String)
                vName = value
            End Set
        End Property

        Public Property LastName() As String
            Get
                Return vLastName
            End Get
            Set(value As String)
                vLastName = value
            End Set
        End Property




    End Structure
End Module
