﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.firstName = New System.Windows.Forms.TextBox()
        Me.lastName = New System.Windows.Forms.TextBox()
        Me.dsks = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Add = New System.Windows.Forms.Button()
        Me.MembersGrid = New System.Windows.Forms.DataGridView()
        Me.Delete = New System.Windows.Forms.Button()
        Me.Edit = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        CType(Me.MembersGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'firstName
        '
        Me.firstName.Location = New System.Drawing.Point(152, 30)
        Me.firstName.Name = "firstName"
        Me.firstName.Size = New System.Drawing.Size(189, 20)
        Me.firstName.TabIndex = 0
        '
        'lastName
        '
        Me.lastName.Location = New System.Drawing.Point(152, 72)
        Me.lastName.Name = "lastName"
        Me.lastName.Size = New System.Drawing.Size(189, 20)
        Me.lastName.TabIndex = 1
        '
        'dsks
        '
        Me.dsks.AutoSize = True
        Me.dsks.Location = New System.Drawing.Point(74, 33)
        Me.dsks.Name = "dsks"
        Me.dsks.Size = New System.Drawing.Size(57, 13)
        Me.dsks.TabIndex = 2
        Me.dsks.Text = "First Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(74, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Last Name"
        '
        'Add
        '
        Me.Add.Location = New System.Drawing.Point(38, 125)
        Me.Add.Name = "Add"
        Me.Add.Size = New System.Drawing.Size(75, 23)
        Me.Add.TabIndex = 5
        Me.Add.Text = "Add"
        Me.Add.UseVisualStyleBackColor = True
        '
        'MembersGrid
        '
        Me.MembersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MembersGrid.Location = New System.Drawing.Point(51, 239)
        Me.MembersGrid.Name = "MembersGrid"
        Me.MembersGrid.ReadOnly = True
        Me.MembersGrid.Size = New System.Drawing.Size(419, 140)
        Me.MembersGrid.TabIndex = 6
        '
        'Delete
        '
        Me.Delete.Location = New System.Drawing.Point(164, 125)
        Me.Delete.Name = "Delete"
        Me.Delete.Size = New System.Drawing.Size(75, 23)
        Me.Delete.TabIndex = 7
        Me.Delete.Text = "Delete"
        Me.Delete.UseVisualStyleBackColor = True
        '
        'Edit
        '
        Me.Edit.Location = New System.Drawing.Point(300, 125)
        Me.Edit.Name = "Edit"
        Me.Edit.Size = New System.Drawing.Size(75, 23)
        Me.Edit.TabIndex = 8
        Me.Edit.Text = "Edit"
        Me.Edit.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(85, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(375, 24)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Gestion des members d'un Club Sportif"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.lastName)
        Me.GroupBox1.Controls.Add(Me.firstName)
        Me.GroupBox1.Controls.Add(Me.Delete)
        Me.GroupBox1.Controls.Add(Me.dsks)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Add)
        Me.GroupBox1.Controls.Add(Me.Edit)
        Me.GroupBox1.Location = New System.Drawing.Point(51, 79)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(419, 154)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(511, 450)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.MembersGrid)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.MembersGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents firstName As TextBox
    Friend WithEvents lastName As TextBox
    Friend WithEvents dsks As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Add As Button
    Friend WithEvents MembersGrid As DataGridView
    Friend WithEvents Delete As Button
    Friend WithEvents Edit As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox1 As GroupBox
End Class
