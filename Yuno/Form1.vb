﻿Public Class Form1
    Private Members As New ArrayList

    Private Function FormValidate() As Boolean
        If firstName.Text.Trim.Equals("") Or lastName.Text.Trim.Equals("") Then
            Return False
        End If
        Return True
    End Function

    Private Sub Add_Click(sender As Object, e As EventArgs) Handles Add.Click
        If FormValidate() = False Then
            MessageBox.Show("One of the inputs are empty, please go check them.")
            Exit Sub
        End If

        Dim member As New Member

        member.Name = firstName.Text
        member.LastName = lastName.Text

        Members.Add(member)

        MembersGrid.DataSource = Nothing
        MembersGrid.DataSource = Members

        firstName.Clear()
        lastName.Clear()

        For i As Integer = 0 To MembersGrid.Columns.Count - 1
            MembersGrid.Columns(i).Width = MembersGrid.Width \ MembersGrid.Columns.Count
        Next



    End Sub

    Private Sub Delete_Click(sender As Object, e As EventArgs) Handles Delete.Click
        Try
            Dim indexHolder = MembersGrid.CurrentRow.Index
            Dim res = MessageBox.Show("Are you sure you want to delete the user ?", "Delete the User", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
            If res = DialogResult.Yes Then

                Members.RemoveAt(indexHolder)

                MembersGrid.DataSource = Nothing
                MembersGrid.DataSource = Members

                For i As Integer = 0 To MembersGrid.Columns.Count - 1
                    MembersGrid.Columns(i).Width = MembersGrid.Width \ MembersGrid.Columns.Count
                Next

            End If
        Catch ex As Exception
            MessageBox.Show("There is no user selected")
        End Try
    End Sub

    Private Sub MembersGrid_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles MembersGrid.CellClick
        Dim p = MembersGrid.CurrentRow.Index
        firstName.Text = MembersGrid.Rows(p).Cells(0).Value.ToString
        lastName.Text = MembersGrid.Rows(p).Cells(1).Value.ToString
    End Sub

    Private Sub Edit_Click(sender As Object, e As EventArgs) Handles Edit.Click
        Dim res = MessageBox.Show("Are you sure you want to update the user ?", "Edit the User", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If res = DialogResult.Yes Then
            If FormValidate() = False Then
                MessageBox.Show("One of the inputs are empty, please go check them.")
                Exit Sub
            End If

            Dim indexHolder = MembersGrid.CurrentRow.Index
            Dim member As New Member

            member.Name = firstName.Text
            member.LastName = lastName.Text

            Members(indexHolder) = member

            MembersGrid.DataSource = Nothing
            MembersGrid.DataSource = Members

            For i As Integer = 0 To MembersGrid.Columns.Count - 1
                MembersGrid.Columns(i).Width = MembersGrid.Width \ MembersGrid.Columns.Count
            Next

        End If
    End Sub
End Class

